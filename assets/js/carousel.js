(function() {
  // Init
  var container = document.getElementById("projects-container"),
    outer = document.getElementById("carousel-container"), 
    reverse = document.getElementById("reverse-container"), 
    inner = document.getElementById("projects-carousel"),
    helper = document.getElementById("helper");

  // Mouse
  var mouse = {
    _x: container.getBoundingClientRect().left,
    _y: container.getBoundingClientRect().top,
    x: 0,
    y: 0,
    updatePosition: function(event) {
      var e = event || window.event;
      this.x = e.clientX - this._x;
      this.y = e.clientY - this._y;
    },
    show: function() {
      return "(" + this.x + ", " + this.y + ")";
    }
  };

  // Track the mouse position relative to the center of the container.
  // mouse.setOrigin(container);

  //--------------------------------------------------

  var counter = 0;
  var updateRate = 1;
  var isTimeToUpdate = function() {
    return counter++ % updateRate === 0;
  };

  //--------------------------------------------------

  var onMouseEnterHandler = function(event) {
    helper.className = "";
    update(event);
  };

  var onMouseLeaveHandler = function() {
    inner.style = "";
    helper.className = "hidden";
    reverse.classList.remove("turn-on");
    outer.classList.remove("turn-on");
  };

  var onMouseMoveHandler = function(event) {
    if (isTimeToUpdate()) {
      update(event);
    }
    displayMousePositionHelper(event);
  };

  //--------------------------------------------------

  var update = function(event) {
    mouse.updatePosition(event);
    if (mouse.x < container.offsetWidth / 3 + container.getBoundingClientRect().left) {
      reverse.classList.remove("turn-on");
      outer.classList.add("turn-on");
    }
    else if (mouse.x > container.offsetWidth * 2/3 + container.getBoundingClientRect().left) {
      outer.classList.remove("turn-on");
      reverse.classList.add("turn-on");
    }
    else {
      reverse.classList.remove("turn-on");
      outer.classList.remove("turn-on");
    }
  };

  var displayMousePositionHelper = function(event) {
    var e = event || window.event;
    helper.innerHTML = mouse.show();
    helper.style = "top:"+(e.clientY-container.offsetTop)+"px;"
                 + "left:"+(e.clientX-container.offsetLeft)+"px;";
  };

  //--------------------------------------------------
  
  var projects = document.getElementsByClassName('project');
  for (var i = 0; i < projects.length; i++) {
    var dataActivate = projects[i].getAttribute('data-activate');
    if (dataActivate) {
      var activateDate = new Date(dataActivate);
      var now = Date.now();
      if (activateDate <= now) {
        projects[i].classList.add('active');
      }
    }
  }

  //--------------------------------------------------

  container.onmouseenter = onMouseEnterHandler;
  container.onmouseleave = onMouseLeaveHandler;
  container.onmousemove = onMouseMoveHandler;

})();