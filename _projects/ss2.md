---
code: ss2
name: Interplanetary Machines
artists:
- Prince Nifty (Matt Smith)
artistbio: Prince Nifty aka Matt Smith has been making music for 15 years. Interplanetary
  Machines will be his 5th full length, following 2020's We're Not In Kansas Anymore,
  2013's Pity Slash Love, 2011's Instant Nifty v ii & i, and 2007's A Sparrow! A Sparrow!
  Along the way, Smith has been busy as a producer and engineer, co-producing Lido
  Pimientas Grammy nominated Miss Colombia, Bernice's Puff LP, Isla Craig's The Becoming,
  and making remixes for Caribou and Sandro Perri among others. He was board president
  of the Toronto based cooperative record label Blocks Recording Club for several
  years.
artistlinks:
- - instagram
  - https://www.instagram.com/prince_nft/
- - bandcamp
  - https://princenifty.bandcamp.com
addtlinfo: Organized by Xenia Benivolski. Album cover painting <em>Sad Clown Anthem</em>
  (2020) by geetha thurairajah. Accompanying text by Fan Wu. Design by Victoria Lum.
  Mastered by Robert Steenkamer.
summary: 'Lyrics for songs in Interplanetary Machines describe a number of dreams
  which were told to Carl Jung by his patients in the process of psychoanalysis, and
  later detailed in Jung''s pivotal book Flying Saucers: a modern myth of things seen
  in the skies (1959). Jung considers sightings of UFOs, whether in dreams or in waking
  life, to be a psychic phenomena, “visionary rumors”, a techno-utopian desire that
  manifests in visions, and a fantastic projection of salvation and meaning onto the
  unknown.'
release: OUT NOW
releasedate: 2022-06-09 22:00:00 +0000
activate: 2022-03-21
buyables:
- SS2A
thumbnail: ss2-photo.jpg
imageMain: ss2/AugustArtRepro_jp0001 v3 Prince Nifty-Interplanetary Machines sRGB
  (1).jpg

---
Lyrics for songs in Interplanetary Machines describe a number of dreams which were told to Carl Jung by his patients in the process of psychoanalysis, and later detailed in Jung's pivotal book Flying Saucers: a modern myth of things seen in the skies (1959). Jung considers sightings of UFOs, whether in dreams or in waking life, to be a psychic phenomena, “visionary rumors”, a techno-utopian desire that manifests in visions, and a fantastic projection of salvation and meaning onto the unknown.

**_Interplanetary Machines_**, recorded between 2013 and 2021, may have been a project born of different times, but its release mid-pandemic feels to be somehow the perfect home for a project that has been plagued by setbacks, health concerns and deep solitary introspection. Nifty writes:

> “I was deep in touring life while trying to make this record happen. Tour life is fun, but it's stressful and disruptive to regular life. I would work on it when I could in home life, but my focus and responsibilities were deeply divided. I slept poorly and had this bizarre and intermittent tinnitus.
>
> By the end of 5 or 6 years of touring my health was waning, and I started losing my singing voice. I had several long bouts with bronchitis and lost the top end of my voice, which is a very difficult place to be as a musician who really relies on their voice, and a nearly impossible place to find yourself in to make a choral record.
>
> I’ve slowly been re-learning and re-training and (hopefully) healing my voice, working in small bursts to complete this record. I realized a couple of years ago that I was instinctively singing the strange droney tinnitus I have, perhaps something akin to what Catherine Christer Hennix calls Raag Tinnitus bringing it to life in the long droning chord passages that stitch this record together, and that somehow has transformed the tinnitus into a kind of inspiration. Music can be a great teacher and healer. Somehow composing around these beautifully terse non-poetic dreamy alien words, these not-my-words felt more profoundly personal and transformational than anything I could have written myself. I think you can hear the complex darkness entangled in the healing brightness in these recordings and maybe that ‘outer space’ is a lot like our inner psychic space.”

{% assign image_path = "assets/images/ss2/Second Spring-Anti-Gravity-Insert-2021-06-21 copy.jpg" %}{% capture lg_image_path %}{{ image_path | resize: '900x900>' }}{% endcapture %}{% capture lq_image_path %}{{ image_path | resize: '100x100>' }}{% endcapture %}{% include lazyload.html image_src=lg_image_path lq_image_src=lq_image_path image_alt="Text by Fan Wu" image_title="Text by Fan Wu" %}
<span class="caption">_Text by Fan Wu_</span>

<div class="embed-element bandcamp-album" style="height: 306px;">
  <iframe style="border: 0; width: 400px; height: 306px;" src="https://bandcamp.com/EmbeddedPlayer/album=3484547819/size=large/bgcol=ffffff/linkcol=0687f5/artwork=small/transparent=true/" seamless><a href="https://princenifty.bandcamp.com/album/interplanetary-machines">Interplanetary Machines by Prince Nifty</a></iframe>
</div>