---
code: ss4
name: 'Desire'
artists:
  - Fortunato Durutti Marinetti
artistbio: "Born in Turin (IT) and based in Toronto (CA), Colussi has been playing in bands of varying degrees of obscurity for the last twenty years,touring North America in his Volvo station wagon and quietly self-releasing albums under various noms de guerre to an oversaturated marketplace.  His first album as Fortunato Durutti Marinetti was 2020’s <strong><em>Desire</em></strong>, which arrived as a limited cassette and quickly sold out.  His next release was <strong><em>Memory’s  Fool</em></strong>, his first time working with proper label support. Second Spring releases Desire for the first time on vinyl, summer 2022. <strong>//</strong> Drawing inspiration from the ruminative, explorative spirit of 1970s songwriters like <strong>Lou Reed</strong>, <strong>Robert Wyatt</strong> and <strong>Joni Mitchell</strong>, Colussi writes music that references baroque pop, jazz, folk, and post-classical archetypes into a form that he calls “poetic jazz rock.” His music is recorded and performed by an evolving crew of sympathetic collaborators. He performs live both solo and with an ensemble."
artistlinks:
  - ['instagram', 'https://www.instagram.com/the_freakers_ball/']
  - ['bandcamp', 'https://fortunatoduruttimarinetti.bandcamp.com']
  - ['soundcloud', 'https://soundcloud.com/user-880886466']
addtlinfo: '<p>Accompanying text by Asesina Hudson, an excerpt from the novella “Blob”. 
Album art by Krisjanis Kaktins-Gorsline. 
Mixed and mastered by Jay Arner.</p>
<p>
Album collaborators: 
Sydney Hermant (Hello Blue Roses)
Nicolas Bragg (Destroyer)
Adrienne Labelle (Supermoon, Milk, various etc.)
Jay Arner (Jay Arner Band, Energy Slime)
Jessica Delisle (Energy Slime)"</p>'
thumbnail: 'ss4/12inch_Jacket_cover_FDM_final.jpg'
imageMain: 'ss4/12inch_Jacket_spine_FDM_final copy.jpg'
release: 2022-09-08
activate: 2022-08-04
buyables:
  - SS4A
---

Originally released as a limited edition cassette in May 2020, Second Spring announces the official vinyl release of ***Desire*** to remedy the fact that the original limited run of cassettes are sold out and to offer listeners the album in the physical format for which these songs were always conceived as being heard. ***Desire*** has been mastered for vinyl and features the original album artwork expanded for vinyl and includes an accompanying text from Asesina Hudson’s novella ***Blob***. 

***Desire*** was recorded intermittently over the winter of 2018/2019 at studios in Vancouver and Winnipeg, and mixed/mastered by Jay Arner. As the first release under the name Fortunato Durutti Marinetti, ***Desire*** also marks a distinct shift towards greater collaboration and expanded instrumentation: violin, cello, keys and backing vocals feature prominently.  Lyrically the album draws on such sources as Joan Didion, Chris Marker films, Georges Battaile, FASTWURMS, Chris Kraus, and Paul Auster in order to ruminate on desire as a part of life that can be both joyful and painful. 

<div class="embed-element bandcamp"><iframe style="border: 0; width: 100%; height: 120px;" src="https://bandcamp.com/EmbeddedPlayer/album=2310367994/size=large/bgcol=ffffff/linkcol=0687f5/tracklist=false/artwork=small/track=4047137833/transparent=true/" seamless><a href="https://fortunatoduruttimarinetti.bandcamp.com/album/desire">Desire by Fortunato Durutti Marinetti</a></iframe></div>

## Press
- “It’s hard not to listen to *Desire* without imagining Fortunato Durutti Marinetti has seen the present when it was the future.” Jim Di Gioia, \[[FULL REVIEW @ **Dominionated**](https://dominionated.ca/reviews/albums/fortunato-durutti-marinetti-desire/)\]
- “A strangely timeless tune with both gravitas and appeal. Featuring backing vocals by Sydney Hermant (Hello Blue Roses), cello by Natanielle Felicitas, and guitar by Nic Bragg (Destroyer)” \[[**Irregular Dreams on “The Bell That Doesn’t Ring”**](https://irregulardreamscanada.wordpress.com/2019/11/25/the-bell-that-doesnt-ring-by-fortunato-durutti-marinetti/)\]
- “Marinetti layers Baroque rhythmic aesthetics onto a folk-rock skeleton, draping the resulting wonder in vocals reminiscent of Lou Reed as he revels in the gentle sway of a flute courtesy of Hermant. The track is a lovely slice of melancholic pop, detailing the consequences of bad advice and misinterpretations when it comes to the awkwardness of various social encounters.” \[[**Beats Per Minute on ‘Everybody Tells Me’**](https://beatsperminute.com/video-premiere-f-d-marinetti-everybody-tells-me/)\]
- “You will hang onto every word that Fortunato Durutti Marinetti quietly utters on “Everybody Tells Me”, a sprawling and gritty song from his new record *Desire*. You will find yourself instinctively leaning into Marinetti as if he suddenly appeared before you, a soft-spoken man. He is frustrated by excess, capitalism, and the advice he receives. He hopes you understand.” [**Dominionated on “Everybody Tells Me”**]
- “Durutti’s poetic approach to his lyrics mixed with the stellar, layered, lush string arrangements makes this album a dynamic collection of romantic, moody & beautiful songs which can stand the test of time. It is clear they have a deep understanding of music, poetry and art, creating something quite incredibly unique in *Desire*.” \[[**Cups N Cakes on Desire**](https://www.cupsncakespod.com/pick-of-the-week/2020/6/4/jordan-klassenfortunato-durutti-marinettithe-golden-age-of-wrestling)\]
- “A quietly visionary art-folk LP led by sole-constant member Daniel Colussi. Their songs hiss and hum with a quiet kind of exasperation that feels truer than the overwrought mystique of contemporaries in this sonic lane.” [**Sled Island**]
- “A finely rendered record that surrenders to the unknown and unknowable...it’s easy to submit to the album’s exquisitely cathartic ride.” \[[**Dominionated on “Desire”**](https://dominionated.ca/reviews/albums/fortunato-durutti-marinetti-desire/)\]