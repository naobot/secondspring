---
code: ss1
name: 'Second Spring - Event - 2019'
artists: 
  - Chanelle Adams
  - Matthew McBride
  - Tiziana La Melia
  - Ellis Sam
  - Kathleen Taylor
  - Igor Santizo
artistbio: "<strong>Chanelle Adams</strong> is a research-based artist, translator, editor, and essayist. After completing a self-guided degree at Brown University, she accepted two Fulbright awards to attend the Ecole des hautes etudes en sciences sociales to research Madagascar plant medicine in French colonial natural history archives. Chanelle co-founded Bluestockings Magazine, was Editor at Black Girl Dangerous, and recently concluded a residency at the Visual Arts Network of South Africa. Her writing on music, ghosts, healing, and diaspora, have been published by NPR, Bitch Media, and The Funambulist, among others.  <a href='http://chanelleadams.info/' target='_blank'>http://chanelleadams.info/</a>. <strong>//</strong> <strong>Tiziana La Melia</strong> is a multi-disciplinary artist. She is the author of <em>The Eyelash and the Monochrome</em> (Talonbooks) and <em>Oral Like Cloaks</em> (Blank Cheque Press). Recent exhibitions include <em>Rust Daughters Say It With Flowers</em>, The Art Gallery of Grande Prairie (2019) and <em>St. Agatha’s Stink Script</em>, anne baurrault, Paris (2019) <em>Global Cows</em>, Damien and the Love Guru, Brussels (2019), and <em>Pastoral Love</em>, Lucas Hirsch, Dusseldorf. <strong>//</strong> <strong>Matthew McBride</strong> was born, lives, and learns on the traditional, ancestral, and unceded territories of the <em>Coast Salish peoples – sḵwx̱wú7mesh (Squamish), sel̓íl̓witulh (Tsleil-Waututh), and xʷməθkʷəy̓əm (Musqueam) nations</em>. He is an educator working for and with the Vancouver School Board and Simon Fraser University that specializes in learning in the early years.  Currently, he teaches Kindergarten, and is interested in Reggio inspired learning environments. <strong>//</strong> <strong>Ellis Sam</strong> explores storytelling through music and video. This year he produced a sound piece for Toronto's Images Festival and exhibited a video for Shadow Tongue at Duplex artist society. He currently resides in Montreal, QC. Most often you can find him playing bass guitar in the romantic pop group Gal Gracen or writing songs as ETcallshome. <strong>//</strong> <strong>Igor Santizo</strong> is a multi-disciplinary artist working on: community arts & facilitation. He has exhibited his own multidisciplinary work within and outside of artist-run cultures. He has also taught art classes, creative process & cultural literacy within diverse settings, including working with youth. And is more recently, applying an integral approach to applied arts and creative development projects: graphic design, murals, etc. Through a long term drawing project entitled Protoic, he has been making many variations of an abstract motif, that is an allusion to primal energy & creative force. One Root many Branches. <strong>//</strong> <strong>Kathleen Taylor</strong> is an artist currently living in Toronto whose performance-based practice combines clown, physical comedy, improvisation, and dance. Through movement, facial expressions, and vocals she generates masks to complicate behaviours and distort the lines between humour, fear, desire, and the grotesque. She holds a BFA from Emily Carr University of Art and Design, Vancouver. She recently participated in Behaviour Swarm: Exploring Performative Practices at the Banff Centre with Michael Portnoy and Kira Nova."

addtlinfo: "Organized by Julian Yi-Zhong Hou with support from Sungpil Yoon and VIVO Media Arts Centre, and the City of Vancouver. With additional collaboration from Julie Zhang, Marika Vandekraats, Aubin Kwon, Sharona Franklin, Joji Fukushima"
releasedate: 2019-10-06
activate: 2019-10-06
buyables:
thumbnail: 'ss1-photo.jpg'
imageMain: 'ss1/second-spring-ilford-xp400-001.jpg'
---


Second Spring invites seven artists, academics and educators to Trout Lake (John Hendry Park) for two days of workshops, lectures and performances. Hand-appliqued textile sigils form the walls of a portable wooden frame, functioning as a stage for events to take place.

In part born out of the interests of real estate speculation in East Vancouver, parks like John Hendry Park emerged out of the “City Beautiful Movement”, which were designed for the livelihood of the workers of extractive industries and to reduce juvenile delinquency. It’s current reputation of having a high coliform count reminds us of the dark histories of the environmental destruction of Vancouver’s early colonial industries which left much of the China Creek flats and False Creek uninhabitable for decades. These obfuscated histories and ecologies that surround the lake form the backdrop out of which a gathering of artists, writers, and educators are brought together to contemplate the invisible communications that occur between plants, animals, and humans. How do forms of invisible communication structure, relay, and describe our adaptation to contradictory landscapes? Second Spring considers suburban park land as generative, fostering all manner of adaptation and mutation. 

# Workshops

## “Runoff, Filtration, & Groundwater” 
 
Workshop with educator Matthew McBride (VAN), suitable for children and adults
 
This workshop experiments with combinations of water and on-site soil samples in the ecosystem to determine which combination produces the most effective filtration. Discussions around erosion and sedimentation poses questions as to how they affect wildlife and human communities within Trout Lake Park. Key Terms and vocabulary:
Water cycle, groundwater recharge, percolation, precipitation, runoff, sedimentation, erosion/erosive force
 
## “The Coyote texts back”

Workshop with artist Igor Santizo (VAN)

Coyote Texts Back aims to engage the intersection between public spheres (plural), the wilderness of the imagination vs the reproduction and enforcement of domestication. By using the blind ubiquity of smartphones, the project by way of: an experiential workshop, writing, twitter texts, and a poster; aims to interject -irreverence and intuitive reflection, to the archetypal coyote both locally real and beyond. Not as a divorced part, but as integrated Being in these most Interdependent and tragic days. // Anthropocene Blues. Are we woke yet?
 
## “Possessive Poisson Poison”

Workshop with writer Chanelle Adams (USA)

A ceremonious experiment in transmission on Trout Lake (networking of knowledge). We will transmit, receive, and exchange around the lake and read aloud from some texts. This is an experiment in the relay of information, not through paper trails but through hidden communications and transference of impressions.
 
 
# Performances
 
**Tiziana La Melia** will be reading poems written in response to modes of visible, invisible and buried communications 1. Corduroy Road: a ballad dedicated to the memory/groundskeeper (Lori Spears) on the rusting agricultural homestead of two scenic artists famous in Beaverlodge Alberta, Euphemia McNaught and her Friend Evelyn McBryan, who followed the construction of the  Alaska Highway. 2. Niscemi Oak Walk: Encounters with cake as a metaphor for territory and time while visiting the ancient oak cork trees that surround a Mobile User Objective System (MUOS) in a Nature Reserve in Niscemi, Sicily. MUOS is a narrowband satellite communication system that employs dish antennas at four worldwide sites to relay secure voice and data communications to military networks and mobile receivers like smartphones. 3. Bubbling up, uncertain and transmitted from Trout Lake and its underground networks of creeks to crack open the final joke, or poem.
 
**Ellis Sam** will be performing as ETcallshome, a songwriting project based around improvisation and chance with digitally generated audio. He gravitates towards the sound of noise inherent in musical recordings, amplifying and composing these frequencies into new rhythmic spaces for wordplay to occur. Alongside some songs, the running thread of this performance will focus on breathing as an instrument and a form of noise. ETcallshome will be working with a jogger in the park to create a live soundscape that could be carried and heard from its auditory epicentre to the lengths of the landscape's perimeter
 
**she-orc** was raised in suburban Ontario by humans. 

{% assign image_path="assets/images/ss1/second-spring-ilford-xp400-003 copy.jpg" %}{% capture lg_image_path %}{{ image_path | resize: '900x900>' }}{% endcapture %}{% capture lq_image_path %}{{ image_path | resize: '100x100>' }}{% endcapture %}{% include lazyload.html image_src=lg_image_path lq_image_src=lq_image_path image_alt="“Possessive Poisson Poison” with Chanelle Adams" image_title="“Possessive Poisson Poison” with Chanelle Adams" %}<span class="caption">*“Possessive Poisson Poison” with Chanelle Adams*</span>

{% assign image_path="assets/images/ss1/second-spring-ilford-xp400-007 copy.jpg" %}{% capture lg_image_path %}{{ image_path | resize: '900x900>' }}{% endcapture %}{% capture lq_image_path %}{{ image_path | resize: '100x100>' }}{% endcapture %}{% include lazyload.html image_src=lg_image_path lq_image_src=lq_image_path image_alt="“Possessive Poisson Poison” with Chanelle Adams" image_title="“Possessive Poisson Poison” with Chanelle Adams" %}<span class="caption">*“Possessive Poisson Poison” with Chanelle Adams*</span>

{% assign image_path="assets/images/ss1/second-spring-ilford-xp400-015 copy.jpg" %}{% capture lg_image_path %}{{ image_path | resize: '900x900>' }}{% endcapture %}{% capture lq_image_path %}{{ image_path | resize: '100x100>' }}{% endcapture %}{% include lazyload.html image_src=lg_image_path lq_image_src=lq_image_path image_alt="“Possessive Poisson Poison” with Chanelle Adams" image_title="“Possessive Poisson Poison” with Chanelle Adams" %}<span class="caption">*“Possessive Poisson Poison” with Chanelle Adams*</span>

{% assign image_path="assets/images/ss1/second-spring-ilford-xp400-005.jpg" %}{% capture lg_image_path %}{{ image_path | resize: '900x900>' }}{% endcapture %}{% capture lq_image_path %}{{ image_path | resize: '100x100>' }}{% endcapture %}{% include lazyload.html image_src=lg_image_path lq_image_src=lq_image_path image_alt="“Coyote texts back” with Igor Santizo" image_title="“Coyote texts back” with Igor Santizo" %}<span class="caption">*“Coyote texts back” with Igor Santizo*</span>

{% assign image_path="assets/images/ss1/second-spring-ilford-xp400-003 copy.jpg" %}{% capture lg_image_path %}{{ image_path | resize: '900x900>' }}{% endcapture %}{% capture lq_image_path %}{{ image_path | resize: '100x100>' }}{% endcapture %}{% include lazyload.html image_src=lg_image_path lq_image_src=lq_image_path image_alt="“Coyote texts back” with Igor Santizo" image_title="“Coyote texts back” with Igor Santizo" %}<span class="caption">*“Coyote texts back” with Igor Santizo*</span>

{% assign image_path="assets/images/ss1/IMG_0278 copy.jpg" %}{% capture lg_image_path %}{{ image_path | resize: '900x900>' }}{% endcapture %}{% capture lq_image_path %}{{ image_path | resize: '100x100>' }}{% endcapture %}{% include lazyload.html image_src=lg_image_path lq_image_src=lq_image_path image_alt="“Runoff, Filtration, & Groundwater” with Matthew McBride" image_title="“Runoff, Filtration, & Groundwater” with Matthew McBride" %}<span class="caption">*“Runoff, Filtration, & Groundwater” with Matthew McBride*</span>

{% assign image_path="assets/images/ss1/IMG_7959 copy.jpg" %}{% capture lg_image_path %}{{ image_path | resize: '900x900>' }}{% endcapture %}{% capture lq_image_path %}{{ image_path | resize: '100x100>' }}{% endcapture %}{% include lazyload.html image_src=lg_image_path lq_image_src=lq_image_path image_alt="“Runoff, Filtration, & Groundwater” with Matthew McBride" image_title="“Runoff, Filtration, & Groundwater” with Matthew McBride" %}<span class="caption">*“Runoff, Filtration, & Groundwater” with Matthew McBride*</span>

{% assign image_path="assets/images/ss1/IMG_6729 copy.jpg" %}{% capture lg_image_path %}{{ image_path | resize: '900x900>' }}{% endcapture %}{% capture lq_image_path %}{{ image_path | resize: '100x100>' }}{% endcapture %}{% include lazyload.html image_src=lg_image_path lq_image_src=lq_image_path image_alt="ETcallshome" image_title="ETcallshome" %}<span class="caption">*ETcallshome*</span>

{% assign image_path="assets/images/ss1/IMG_6733 copy.jpg" %}{% capture lg_image_path %}{{ image_path | resize: '900x900>' }}{% endcapture %}{% capture lq_image_path %}{{ image_path | resize: '100x100>' }}{% endcapture %}{% include lazyload.html image_src=lg_image_path lq_image_src=lq_image_path image_alt="ETcallshome" image_title="ETcallshome" %}
<span class="caption">*ETcallshome*</span>

## Press
- [*Second Spring*, VIVO Media Arts](https://www.vivomediaarts.com/programming/exhibition/second-spring)