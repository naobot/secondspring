---
code: ss3
name: 'Grass Drama & Selected Works'
artists:
  - Julian Yi-Zhong Hou
artistbio: "<strong>Julian Yi-Zhong Hou</strong> (b. 1980) is a multidisciplinary artist born in Edmonton, Alberta, Treaty 6 territory, and currently resides in Vernon, on the unceded land of the Syilx peoples of the Okanagan Nation. His work centres around contemporary mystical subjects including consciousness, divination ritual, practice, and research, and symbology. Grass Drama has been shown in parts at Malaspina Printmakers Society, Vancouver (2021), Contemporary Art Gallery, Vancouver (2020); Unit 17, Vancouver (2018); and in Charcuterie 4 (2018). He has held residencies at Triangle, Marseille; Western Front and 221A Vancouver, and in 2017 won the City of Vancouver’s Mayor’s Award for Emerging Visual Artist. He was long-listed for the Sobey Award in 2021. He has previously released records alongside collaborator Michael Loncaric as the Stick."
artistlinks:
  - ['website', 'https://julianhou.com/']
  - ['instagram', 'https://www.instagram.com/julian_hou/']
  - ['bandcamp', 'https://julianyi-zhonghou.bandcamp.com/releases']
  - ['soundcloud', 'https://soundcloud.com/julianhou']
addtlinfo:
  'The record was produced by Ellis Sam, mastered by Jonathan Scherk, and pressed by PCR.'
releasedate: 2020-10-31
activate: 2020-10-31
buyables:
  - SS3A
  - SS3E
  - SS3F
  - SS3G
  - SS3H
  - SS3I
  - SS3J
  - SS3K
thumbnail: ss3-photo.jpg
imageMain: 'ss3/Copy of Grass Drama_Gatefold Artwork_Front and Back.jpg'
summary: "Grass Drama is an artwork by Julian Yi-Zhong Hou that spanned multiple exhibition spaces, publications, ephemera, and performances and centres around a gatefold double LP which includes Selected Works, a collection of previously unreleased audio works which have accompanied the artist's exhibition installations in the past 6 years. Grass Drama draws together a diverse set of influences including bedroom folk recordings, medieval composition sensibilities, timbres and textures from Chinese soap operas, soundscape, and midi orchestration."
---

Grass Drama is a ritual-based performance that spans multiple sites, temporalities, and formats. Centred around a 7-day THOTH tarot reading that took place through the winter solstice of 2017, the full length vinyl record LP explores layers of symbolism toward the production of new mythologies and manifestations. Grass Drama layers together bedroom folk recordings, medieval composition sensibilities, timbres and textures from Chinese soap operas, and midi orchestration. Previous iterations of Grass Drama have been shown in several exhibition spaces including the Contemporary Art Gallery (Vancouver), Malaspina Printmakers Society (Vancouver), Unit 17 (Vancouver), Cassandra Cassandra (Toronto) and in the publication Charcuterie (Vancouver). 

The record also includes a second LP of previous and related sound works made between 2014-2018 titled Selected Works.

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/678893789?h=716dfd7366" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
<span class="caption">*Passionate Daylight*, Video by Fleur Melbourn</span>

<div class="embed-element bandcamp-album"> 
<iframe style="border: 0; width: 100%; height: 472px;" src="https://bandcamp.com/EmbeddedPlayer/album=2983013445/size=large/bgcol=ffffff/linkcol=0687f5/artwork=small/transparent=true/" seamless><a href="https://julianyi-zhonghou.bandcamp.com/album/selected-works-2014-2018">Selected Works (2014-2018) by Julian Yi-Zhong Hou</a></iframe>
</div>

## Press
- “The LP strikes a gentle balance between simple, elegantly crafted songs and nuanced narrative soundscapes. However, this exterior impression of effortless equilibrium contains within it an impossible radiant geometry of meaning and association, segments of which surface inconspicuously within lyrical content, symbology, and imagination.” \[**Magnus Tiesenhausen, Album Review, CMagazine Issue 149, 2021**\]
- “*Grass Drama* is an immersive listening experience that I keep returning to because, although the album unfurls slowly, there is so much to absorb, and I don’t want to miss anything.” \[[**Laura Stanley, Album Review, New Feeling, 2021**](https://newfeeling.ca/2021/08/24/julian-yi-zhong-hou-grass-drama-selected-works)\]

## Exhibitions
- [Staying in the feeling at Malaspina Printmakers Society](https://www.malaspinaprintmakers.com/julian-hou-staying-in-the-feeling.html)
- [Grass Drama at the Contemporary Art Gallery](https://www.contemporaryartgallery.ca/exhibitions/julianyijonghou)