---
layout: page-content
title: About
permalink: "/about/"

---
Second Spring is a growing affiliation of collaborators and an artist imprint that produces vinyl records, online projects, and events. Second Spring is currently located in Vernon, B.C. Canada on the unceded territory of the Syilx people of the Okanagan Nation. Our primary focus is, but not limited to, all things panpsychic.

Contact us using our <a href="https://us14.list-manage.com/contact-form?u=64cffd5c410a34aa48a832464&form_id=c75b49837b71fa306da186c496bc7b73" target="_blank">contact form</a> or directly at <a href="mailto:{{ site.data.links.contact }}">**{{ site.data.links.contact }}**</a>.