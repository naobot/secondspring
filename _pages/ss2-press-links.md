---
layout: main
---

<section class="section">
  <div class="press-link has-text-centered mb-4">Press kit:&nbsp;<a href="https://drive.google.com/drive/folders/1ndrO9hwPovFcCKNQgAFTWBfMtNFD-YvW?usp=sharing" target="_blank">Prince Nifty - Interplanetary Machines - Electronic Press Kit</a></div>
  <div class="has-text-centered">See also:&nbsp;<a href="{{ site.baseurl }}ss2">Project page</a></div>
</section>