---
layout: main
---

<section class="section">
  <div class="press-link has-text-centered mb-4">Press kit:&nbsp;<a href="https://drive.google.com/drive/folders/1K0ySoqJSrJSrneWKgWIHrrBKiSXGVzWu?usp=sharing" target="_blank">Fortunato Durutti Marinetti - Desire - Electronic Press Kit</a></div>
  <div class="has-text-centered">See also:&nbsp;<a href="{{ site.baseurl }}ss4">Project page</a></div>
</section>