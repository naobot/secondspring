---
event_code: 'e1'
project: 'ss5'
poster: 'ss5/SS5_Poster_Final.png'
startDate: 2022-06-06
endDate: 2022-06-10
location: 'The Music Gallery'
event_title: 'STRANGE RELIEF - JUNE 6-10 2022'
ticket_link: https://www.showclix.com/event/strange-relief
---

## Sound installation 

*Strange Relief: Panpsychic Sound Floor* by Julian Yi-Zhong Hou 

**Open June 7-9, noon-8pm daily**
 
In this developing speculative work, Hou uses the psychedelic technology Ketamine for the speculative mapping of inner space into architecture, sound, and writing. Panpsychic Sound Floor is one floor within this larger work, and interweaves ideas around auditory panpsychism and dissociative invocations. 
 
## Talk

*Distancing Mechanisms: Ketamine & Daoism* by Fan Wu

**June 6, 6:30-8pm**
 
In this talk and public forum, Fan Wu explores a speculative comparison: How do the strange bedfellows of ketamine and Daoism both manifest necessary distance from such sticky substances as ego, attachment, and knowledge? Thinking through John C. Lilly, Zhuangzi, and private phenomenological accounts of ketamine use, how can this distance be the foundation for spiritual and ethical experience? We will all walk together through Julian Hou's Strange Relief to embody these questions through sound & sensoria.
 
## Panel

*Wandering in the Wild: a conversation on mental health in the music industry* organized and lead by Al Spx

**June 8, 8pm**
 
The music industry is an incredibly difficult one to navigate. With hectic tour schedules and pressures associated with albums, more and more artists are struggling to manage their mental health. We will touch on subjects such as touring, releasing projects, and management and how we manage to stay sane throughout it all. 
 
Al Spx will be facilitating the conversation. She has a history of psychosis that was triggered by a relentless tour. For her, the music industry has been detrimental to her mental health and there is a great deal to touch on with this conversation.
 
 
## Event/Record Launch 

[*Prince Nifty: Interplanetary Machines record launch* ](https://musicgallery.org/events/strange-relief-part-4/)

**June 10, Doors 7:30pm, Starts 8pm**

Performances by Prince Nifty and Stefana Fratila, and sound installation by Julian Yi-Zhong Hou
 
$35 Record & Ticket Combo / $15 Advanced / $10 Accessibility ($20 at the door)
 
**7:30pm** - Doors and Installation *Strange Relief: Panpsychic Sound Floor* by Julian Yi-Zhong Hou (ambient) 
 
**8:30pm** - *Interplanetary Interludes* by Stefana Fratila
 
Stefana Fratila will perform *Interplanetary interludes* (soundscapes made up of weather from the planets in our solar system) in the sunroom.
 
**9:00pm** Live performance by Prince Nifty for the launch of his new record *Interplanetary Machines* 
 
Matt Smith, Prince Nifty, will perform experimental works that segue from his *Interplanetary Machines* record toward a new related album work.
