---
event_code: 'e1'
project: 'ss1'
poster: 'ss1/second_spring_poster_final2_websize.jpg'
startDate: 2019-10-06
endDate: 2019-10-20
location: 'Trout Lake'
event_title: 'SECOND SPRING, a series of free workshops and performances at TROUT LAKE on October 6th and October 20th, 2019'
---

Organized by Julian Hou and Sungpil Yoon as part of VIVO Media Arts Centre, and with support from the City of Vancouver and Vancouver Board of Parks and Recreation.

{% assign image_path="assets/images/ss1/ssT2019Edit.jpg" %}{% capture lg_image_path %}{{ image_path | resize: '900x900>' }}{% endcapture %}{% capture lq_image_path %}{{ image_path | resize: '100x100>' }}{% endcapture %}{% include lazyload.html image_src=lg_image_path lq_image_src=lq_image_path %}
 
## Location:
 
As guests on the Unceded Coast Salish Territories of the xʷməθkwəy̓əm (Musqueam), Skwxwú7mesh (Squamish) and Səl̓ílwətaʔ/Selilwitulh (Tsleil-Waututh) Nations, on the eastern side of Trout Lake (John Hendry Park), by the water and the willow trees. 
 
## Details:
 
Second Spring invites seven artists, academics and educators to Trout Lake (John Hendry Park) for two days of workshops, lectures and performances. Hand-appliqued textile sigils form the walls of a portable wooden frame, functioning as a stage for events to take place.

In part born out of the interests of real estate speculation in East Vancouver, parks like John Hendry Park emerged out of the “City Beautiful Movement”, which were designed for the livelihood of the workers of extractive industries and to reduce juvenile delinquency. Its current reputation of having a high coliform count reminds us of the dark histories of the environmental destruction of Vancouver’s early colonial industries which left much of the China Creek flats and False Creek uninhabitable for decades. These obfuscated histories and ecologies that surround the lake form the backdrop out of which a gathering of artists, writers, and educators are brought together to contemplate the invisible communications that occur between plants, animals, and humans. How do forms of invisible communication structure, relay, and describe our adaptation to contradictory landscapes? Second Spring considers suburban park land as generative, fostering all manner of adaptation and mutation. 

## Workshops:

### October 6, 2019, Sunday

12:00-1:15pm
**“Runoff, Filtration, & Groundwater”**
Workshop with educator Matthew McBride (VAN), suitable for children and adults

This workshop experiments with combinations of water and on-site soil samples in the ecosystem to determine which combination produces the most effective filtration. Discussions around erosion and sedimentation poses questions as to how they affect wildlife and human communities within Trout Lake Park. 

Key Terms and vocabulary: Water cycle, groundwater recharge, percolation, precipitation, runoff, sedimentation, erosion/erosive force
 
 
2:00-3:15pm
**“The Coyote texts back”**
Workshop with artist Igor Santizo (VAN)
Coyote Texts Back aims to engage the intersection between public spheres (plural), the wilderness of the imagination vs the reproduction and enforcement of domestication. By using the blind ubiquity of smartphones, the project by way of: an experiential workshop, writing, twitter texts, and a poster; aims to interject -irreverence and intuitive reflection, to the archetypal coyote both locally real and beyond. Not as a divorced part, but as integrated Being in these most Interdependent and tragic days. // Anthropocene Blues. Are we woke yet?
 
4:00-6:00pm
**“Possessive Poisson Poison”**
Workshop with writer Chanelle Adams (USA)
A ceremonious experiment in transmission on Trout Lake (networking of knowledge). We will transmit, receive, and exchange around the lake and read aloud from some texts. This is an experiment in the relay of information, not through paper trails but through hidden communications and transference of impressions.
 
Please contact events@vivomediaarts.com to attend workshops and for other inquiries

To book online vivo-second-spring.eventbrite.com
 
 
## Performances:
 
### October 6, 2019, Sunday 6:15pm
 
**Tiziana La Melia** will be reading poems written in response to modes of visible, invisible and buried communications 1. Corduroy Road: a ballad dedicated to the memory/groundskeeper (Lori Spears) on the rusting agricultural homestead of two scenic artists famous in Beaverlodge Alberta, Euphemia McNaught and her Friend Evelyn McBryan, who followed the construction of the  Alaska Highway. 2. Niscemi Oak Walk: Encounters with cake as a metaphor for territory and time while visiting the ancient oak cork trees that surround a Mobile User Objective System (MUOS) in a Nature Reserve in Niscemi, Sicily. MUOS is a narrowband satellite communication system that employs dish antennas at four worldwide sites to relay secure voice and data communications to military networks and mobile receivers like smartphones. 3. Bubbling up, uncertain and transmitted from Trout Lake and its underground networks of creeks to crack open the final joke, or poem.
 
**Ellis Sam** will be performing as ETcallshome, a songwriting project based around improvisation and chance with digitally generated audio. He gravitates towards the sound of noise inherent in musical recordings, amplifying and composing these frequencies into new rhythmic spaces for wordplay to occur. Alongside some songs, the running thread of this performance will focus on breathing as an instrument and a form of noise. ETcallshome will be working with a jogger in the park to create a live sound scape that could be carried and heard from its auditory epicentre to the lengths of the landscape's perimeter
 
**she-orc** was raised in suburban Ontario by humans. 
 
 
### October 20, 2019, Sunday 4pm
 
*Using the purpose built structure as a stage, Julian Hou will perform "Second Spring", a rehearsal of a single scene of a longer performative work to be staged in 2020 in various locations.* This work navigates around a variety of subjects which utilize the history of bamboo in furniture design and architecture, playing cards and divination, a historical perspective on the opium wars, tobacco manufacture and cigarette smoking, and other themes as ways to articulate colonial histories. It is a project that traces the interconnectivity between scales, temporalities, and materials within a non-western epistemological framework. It proposes a mutation of narrative, describing the trajectories of invented and recombinant subjectivities of heterotopic conditions that are both resistant to and are interpellated by capitalist ideologies.
 
 
For more information please visit: vivomediaarts.com/programming/exhibition/second-spring
 
## Biographies
 
**Chanelle Adams** is a research-based artist, translator, editor, and essayist. After completing a self-guided degree at Brown University, she accepted two Fulbright awards to attend the Ecole des hautes etudes en sciences sociales to research Madagascar plant medicine in French colonial natural history archives. Chanelle co-founded Bluestockings Magazine, was Editor at Black Girl Dangerous, and recently concluded a residency at the Visual Arts Network of South Africa. Her writing on music, ghosts, healing, and diaspora, have been published by NPR, Bitch Media, and The Funambulist, among others.  http://chanelleadams.info/.
 
**Julian Hou** is a Vancouver based artist whose practice involves the development of audio fictions that integrate music with spoken language, hand made costume and printed clothing, as well as pattern-based digital collages rooted in drawing and design. Evoking affective states, mis-memory, magic, threshold consciousness and thought-forms, works are assembled into installations that engage psychophysiological experiences of the body and it’s extension into clothing and architectural interiors. Most rrecently he exhibited his work at Cassandra Cassandra in Toronto, and participated in a residency at Triangle in Marseille. His work has been previously shown at the Vancouver Art Gallery (2016-2017), Artspeak, Vancouver (2017); 8eleven, Toronto (2017); Things that can happen, Hong Kong (2017); and the Audain gallery, Vancouver (2015).
 
**Tiziana La Melia** is a multi-disciplinary artist. She is the author of The Eyelash and the Monochrome (Talonbooks) and Oral Like Cloaks (Blank Cheque Press). Recent exhibitions include Rust Daughters Say It With Flowers, The Art Gallery of Grande Prairie (2019) and St. Agatha’s Stink Script, anne baurrault, Paris (2019) Global Cows, Damien and the Love Guru, Brussels (2019), Pastoral Love, Lucas Hirsch, Dusseldorf.
 
**Matthew McBride** was born, lives, and learns on the traditional, ancestral, and unceded territories of the *Coast Salish peoples – sḵwx̱wú7mesh (Squamish), sel̓íl̓witulh (Tsleil-Waututh), and xʷməθkʷəy̓əm (Musqueam) nations*. He is an educator working for and with the Vancouver School Board and Simon Fraser University that specializes in learning in the early years.  Currently, he teaches Kindergarten, and is interested in Reggio inspired learning environments.
 
**Ellis Sam** explores storytelling through music and video. This year he produced a sound piece for Toronto's Images Festival and exhibited a video for Shadow Tongue at Duplex artist society. He currently resides in Montreal, QC. Most often you can find him playing bass guitar in the romantic pop group Gal Gracen or writing songs as ETcallshome.
 
**Igor Santizo** is a multi-disciplinary artist working on: community arts & facilitation. He has exhibited his own multidisciplinary work within and outside of artist-run cultures. He has also taught art classes, creative process & cultural literacy within diverse settings, including working with youth. And is more recently, applying an integral approach to applied arts and creative development projects: graphic design, murals, etc. Through a long term drawing project entitled Protoic, he has been making many variations of an abstract motif, that is an allusion to primal energy & creative force. One Root many Branches.
 
**Kathleen Taylor** is an artist currently living in Toronto whose performance-based practice combines clown, physical comedy, improvisation, and dance. Through movement, facial expressions, and vocals she generates masks to complicate behaviours and distort the lines between humour, fear, desire, and the grotesque. She holds a BFA from Emily Carr University of Art and Design, Vancouver. She recently participated in Behaviour Swarm: Exploring Performative Practices at the Banff Centre with Michael Portnoy and Kira Nova.
 
***Additional acknowledgements:*** Julie Zhang, Marika Vandekraats, Aubin Kwon, Sharona Franklin, Joji Fukushima

