---
code: SS3H
project: ss3
name: '7 7 7 shirt'
price: 44
images:
  - 'SS3H copy.jpg'
info: 'Julian Yi-Zhong Hou is an artist born in Edmonton, AB, Treaty 6 territory, and currently lives in Vernon, B.C., on the unceded land of the Syilx People of the Okanagan Nation. His expanded practice interweaves contemporary psychedelia, synaesthesia, magic traditions and conceptual & sacred geometry.'
shopify_component_id: '1652126840352'
shopify_product_id: '7628316606682'
---

Black on white T shirt 
