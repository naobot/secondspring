---
code: SS3J
project: ss3
name: 'Triple Spiral Root Chakra Shirt'
price: 44
images:
  - 'SS3J-1 copy.jpg'
  - 'SS3J-2 copy.jpg'
info: 'Julian Yi-Zhong Hou is an artist born in Edmonton, AB, Treaty 6 territory, and currently lives in Vernon, B.C., on the unceded land of the Syilx People of the Okanagan Nation. His expanded practice interweaves contemporary psychedelia, synaesthesia, magic traditions and conceptual & sacred geometry.'
shopify_component_id: '1652126928495'
shopify_product_id: '7628394758362'
---

Gold on white T shirt 
