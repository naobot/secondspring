---
code: SS3K
project: ss3
name: 'Triple Spiral Root Chakra Shirt'
price: 44
images:
  - 'SS3K-1 copy.jpg'
  - 'SS3K-2 copy.jpg'
info: 'Julian Yi-Zhong Hou is an artist born in Edmonton, AB, Treaty 6 territory, and currently lives in Vernon, B.C., on the unceded land of the Syilx People of the Okanagan Nation. His expanded practice interweaves contemporary psychedelia, synaesthesia, magic traditions and conceptual & sacred geometry.'
shopify_product_id: '7628424675546'
shopify_component_id: '1652127041064'
---

White on black T shirt 
