---
code: SS2A
project: ss2
name: Interplanetary Machines
price: "30"
images:
- IMG_0358.jpg
- IMG_0363.jpg
- IMG_0357.jpg
- IMG_0348.jpg
release: AVAILABLE NOW
shopify_product_id: "7592482537690"
shopify_component_id: "1651346914112"

---
Vinyl Record