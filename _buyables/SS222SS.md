---
code: SS222SS
name: 'SSSummerSolsticeMixtape222: FACE/OFF'
price: 22.22
images:
  - 'SSSummerSolsticeMixTape222front.jpg'
  - 'SSSummerSolsticeMixTape_back.jpg'
info: 'A summer mixtape with bilateral symmetry of the mind and those entirely fused or complementary at its core, this mix inaugerates a series of mixtapes to be released by Second Spring this summer.'
shopify_product_id: '7674849296602'
shopify_component_id: '1655866157690'
---

2-pack of 90 minute cassette tapes. Limited edition (22 available).

A summer mixtape with bilateral symmetry of the mind and those entirely fused or complementary at its core, this mix inaugerates a series of mixtapes to be released by Second Spring this summer. Two 90 minute cassettes with tracks selected by friends and associates of Second Spring.