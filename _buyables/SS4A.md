---
code: SS4A
project: ss4
name: Fortunato Durutti Marinetti - Desire
price: "25"
images:
- SS4_covercopy.jpg
- SS4_label.jpg
- SS4_backcopy.jpg
shopify_product_id: "7592492663002"
shopify_component_id: "1659550479086"
info: |-
  Originally released as a limited edition cassette in May 2020, Second Spring announces the official vinyl release of <strong><em>Desire</em></strong> to remedy the fact that the original limited run of cassettes are sold out and to offer listeners the album in the physical format for which these songs were always conceived as being heard. <strong><em>Desire</em></strong> has been mastered for vinyl and features the original album artwork expanded for vinyl and includes an accompanying text from Asesina Hudson’s novella <strong><em>Blob</em></strong>.
  <strong><em>Desire</em></strong> was recorded intermittently over the winter of 2018/2019 at studios in Vancouver and Winnipeg, and mixed/mastered by Jay Arner. As the first release under the name Fortuanto Durutti Marinetti, <strong><em>Desire</em></strong> also marks a distinct shift towards greater collaboration and expanded instrumentation: violin, cello, keys and backing vocals feature prominently.  Lyrically the album draws on such sources as Joan Didion, Chris Marker films, Georges Battaile, FASTWURMS, Chris Kraus, and Paul Auster in order to ruminate on desire as a part of life that can be both joyful and painful.

---
Vinyl Record