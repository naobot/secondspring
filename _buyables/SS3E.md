---
code: SS3E
project: ss3
name: '7 7 7 shirt'
price: 44
images:
  - 'SS3E.jpg'
info: 'Julian Yi-Zhong Hou is an artist born in Edmonton, AB, Treaty 6 territory, and currently lives in Vernon, B.C., on the unceded land of the Syilx People of the Okanagan Nation. His expanded practice interweaves contemporary psychedelia, synaesthesia, magic traditions and conceptual & sacred geometry.'
shopify_component_id: '1651347082401'
shopify_product_id: '7592504557786'
---

Pale mint on white T shirt 
