---
code: SS3A
project: ss3
name: 'Grass Drama & Selected Works'
price: 30
images:
  - 'IMG_0365.jpg'
  - 'IMG_0373.jpg'
  - 'IMG_0370.jpg'
info: 'Julian Yi-Zhong Hou is an artist born in Edmonton, AB, Treaty 6 territory, and currently lives in Vernon, B.C., on the unceded land of the Syilx People of the Okanagan Nation. His expanded practice interweaves contemporary psychedelia, synaesthesia, magic traditions and conceptual & sacred geometry.'
release: 2021-10-31
shopify_product_id: '7592490270938'
shopify_component_id: '1651347025636'
---

Double LP Vinyl Record