---
code: SS3I
project: ss3
name: 'Triple Spiral Root Chakra T-shirt'
price: 44
images:
  - 'SS3i-1 copy.jpg'
  - 'SS3i-2 copy.jpg'
info: 'Julian Yi-Zhong Hou is an artist born in Edmonton, AB, Treaty 6 territory, and currently lives in Vernon, B.C., on the unceded land of the Syilx People of the Okanagan Nation. His expanded practice interweaves contemporary psychedelia, synaesthesia, magic traditions and conceptual & sacred geometry.'
shopify_component_id: '1652126982122'
shopify_product_id: '7628419236058'
---

Black on white T shirt 
