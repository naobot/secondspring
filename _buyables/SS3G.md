---
code: SS3G
project: ss3
name: '7 7 7 shirt'
price: 44
images:
  - 'SS3G copy.jpg'
info: 'Julian Yi-Zhong Hou is an artist born in Edmonton, AB, Treaty 6 territory, and currently lives in Vernon, B.C., on the unceded land of the Syilx People of the Okanagan Nation. His expanded practice interweaves contemporary psychedelia, synaesthesia, magic traditions and conceptual & sacred geometry.'
shopify_product_id: '7628343804122'
shopify_component_id: '1652126766194'
---

White on black T shirt 
